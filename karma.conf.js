// Karma configuration file, see link for more information
// https://karma-runner.github.io/0.13/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular/cli'],

    reporters : config.angularCli && config.angularCli.codeCoverage
                  ? ['progress', 'junit', 'coverage', 'mocha', 'karma-remap-istanbul']
                  : ['progress', 'junit', 'coverage', 'mocha', 'karma-remap-istanbul'],

    port: 8765,
    colors: true,
    logLevel: config.LOG_INFO,

    // don't watch for file change
    autoWatch        : true,
    // only runs on headless browser
    browsers         : ['Chrome'],
    // just run one time
    singleRun        : false,
    // remove `karma-chrome-launcher` because we will be running on headless
    // browser on Jenkins

    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-junit-reporter'),
      require('karma-coverage'),
      require('karma-jenkins-reporter'),
      require('karma-mocha-reporter'),
      require('karma-remap-istanbul'),
      require('@angular/cli/plugins/karma')
    ],

    // changes type to `cobertura`
    coverageReporter : {
      type : 'cobertura',
      dir  : 'target/coverage-reports/'
    },

    // saves report at `target/surefire-reports/TEST-*.xml` because Jenkins
    // looks for this location and file prefix by default.
    junitReporter    : {
      outputDir : 'target/surefire-reports/'
    },

    remapIstanbulReporter: {
      reports: {
        html: 'coverage',
        lcovonly: './coverage/coverage.lcov'
      }
    },

    files: [
      { pattern: './src/test.ts', watched: false }
    ],

    preprocessors: {
      './src/test.ts': ['@angular/cli'],
    },

    mime: {
      'text/x-typescript': ['ts','tsx']
    },

    angularCli: {
      config: './angular-cli.json',
      environment: 'dev',
    }
  });
};
