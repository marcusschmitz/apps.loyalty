import { HTTP } from '@ionic-native/http';
import { Network } from '@ionic-native/network';

import { AuthService } from './auth.service';
import { HttpServiceMock } from './http.service.mock';
import { StorageServiceMock } from './storage.service.mock';
import { UtilsService } from './utils.service';

describe('Service: Auth', () => {

    let auth: AuthService;

    beforeEach(() => {
        auth = new AuthService(
            (<any> new HttpServiceMock(
                (<any> new HTTP()),
                (<any> new Network())
            )),
            (<any> new StorageServiceMock()),
            (<any> new UtilsService())
        );
    });

    it('initialises', () => {
        expect(auth).not.toBeNull();
    });

    it('logs successfully in', (done: Function) => {
        auth.login('valid', 'request')
            .then(() => {
                expect(auth.credentials.access_token).toBe('access_token');
                expect(auth.credentials.refresh_token).toBe('refresh_token');
                expect(auth.credentials.expires_in).toBe(1000);
                expect(auth.credentials.token_type).toBe('token_type');

                done();
            });
    });

    it('handles invalid request', (done: Function) => {
        auth.login('invalid', 'request')
            .catch((error: any) => {
                expect(error.title).toBe('LOYALTY_APP.LOGIN.MESSAGE.INVALID.TITLE');
                expect(error.error).toBe('LOYALTY_APP.LOGIN.MESSAGE.INVALID.TEXT');

                done();
            });
    });

    it('detects offline request', (done: Function) => {
        auth.login('offline', 'request')
            .catch((error: any) => {
                expect(error.title).toBe('LOYALTY_APP.ERROR.HTTP.OFFLINE.TITLE');
                expect(error.error).toBe('LOYALTY_APP.ERROR.HTTP.OFFLINE.TEXT');

                done();
            });
    });

    it('handles failing request', (done: Function) => {
        auth.login('error', 'request')
            .catch((error: any) => {
                expect(error.title).toBe('LOYALTY_APP.ERROR.HTTP.REQUEST');
                expect(error.error).toBe('timeout');

                done();
            });
    });

    it('logs out', (done: Function) => {
        auth.setCredentials('foobar');
        expect(auth.credentials).not.toBe(null);

        auth.logout()
            .then(() => {
                expect(auth.credentials).toBe(null);

                done();
            });
    });

    it('detects if not logged in', (done: Function) => {
        auth.isLoggedIn().catch(() => done());
    });

    it('detects if logged in', (done: Function) => {
        auth.login('valid', 'request')
            .then(() => {
                auth.isLoggedIn().then(() => done());
            });
    });

    it('get login data from storage', (done: Function) => {
        auth.login('valid', 'request')
            .then(() => {
                auth.setCredentials(null);
                auth.isLoggedIn().then(() => done());
            });
    });

    it('logs in again if token is expired', (done: Function) => {
        auth.login('valid', 'request')
            .then(() => {
                auth.credentials.expires  = 0;
                auth.http['userInfoCall'] = 0;

                auth.isLoggedIn().then(() => done());
            });
    });

    it('gets access token', (done: Function) => {
        let token: string = auth.getAccessToken();

        expect(token).toBe('');

        auth.login('valid', 'request')
            .then(() => {
                token = auth.getAccessToken();

                expect(token).toBe('access_token');

                done();
            });
    });

    it('gets refresh token', (done: Function) => {
        let token: string = auth.getRefreshToken();

        expect(token).toBe('');

        auth.login('valid', 'request')
            .then(() => {
                token = auth.getRefreshToken();

                expect(token).toBe('refresh_token');

                done();
            });
    });

    it('gets header', (done: Function) => {
        let header: any;

        auth.login('valid', 'request')
            .then(() => {
                header = auth.getHeader('basic', false);
                expect(header['Authorization']).toBe('loyalty-app-fr//KwdLCXAisee6(Vm3FWrkey#kYTwdxQgkgdb');

                expect(header['X-Brand']).not.toBe('');
                expect(header['X-Country']).not.toBe('');
                expect(header['X-Language']).not.toBe('');
                expect(header['X-Middleware-Token']).not.toBe('');
                expect(header['Content-Type']).toBe(undefined);

                header = auth.getHeader('access', false);
                expect(header['Authorization']).toBe('Bearer access_token');

                header = auth.getHeader('refresh', false);
                expect(header['Authorization']).toBe('Bearer refresh_token');

                header = auth.getHeader('access', true);
                expect(header['Content-Type']).toBe('application/json; charset=utf-8');

                done();
            });
    });

    it('gets user info', (done: Function) => {
        auth.getUserInfo()
            .then((userInfo: any) => {
                expect(userInfo.firstname).toBe('John');
                expect(userInfo.lastname).toBe('Doe');

                auth.getUserInfo()
                    .catch((error: any) => {
                        expect(error.title).toBe('LOYALTY_APP.ERROR.HTTP.REQUEST');
                        expect(error.error).toBe('LOYALTY_APP.ERROR.JSON.PARSE');

                        auth.getUserInfo()
                            .catch((error: any) => {
                                expect(error.title).toBe('LOYALTY_APP.ERROR.HTTP.OFFLINE.TITLE');
                                expect(error.error).toBe('LOYALTY_APP.ERROR.HTTP.OFFLINE.TEXT');

                                done();
                            });
                    });
            });
    });

    it('checks if loyalty', () => {
        let isLoyalty: boolean;
        let userInfo: any = {user: {account: {}}};

        isLoyalty = auth.isLoyalty(userInfo);
        expect(isLoyalty).toBeFalsy();

        userInfo.user.account.loyalty = null;
        isLoyalty = auth.isLoyalty(userInfo);
        expect(isLoyalty).toBeFalsy();

        userInfo.user.account.loyalty = {};
        isLoyalty = auth.isLoyalty(userInfo);
        expect(isLoyalty).toBeFalsy();

        userInfo.user.account.loyalty.id = null;
        isLoyalty = auth.isLoyalty(userInfo);
        expect(isLoyalty).toBeFalsy();

        userInfo.user.account.loyalty.id = 1;
        isLoyalty = auth.isLoyalty(userInfo);
        expect(isLoyalty).toBeTruthy();
    });
});
