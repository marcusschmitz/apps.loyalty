import { HTTP } from '@ionic-native/http';
import { TranslateMock } from '../mocks';

import { HttpService } from './http.service';

describe('Service: Http', () => {

    let http: HttpService;
    let warn: any;

    beforeAll(() => {
        // Disable warning due to cordova missing
        warn = console.warn;
        console.warn = (): void => {};
    });

    afterAll(() => {
        console.warn = warn;
    });

    beforeEach(() => {
        http = new HttpService(
            (<any> new HTTP()),
            (<any> new TranslateMock())
        );
    });

    it('initialises', () => {
        expect(http).not.toBeNull();
    });

    it('makes GET request', (done: Function) => {
        spyOn(http, 'isOffline').and.returnValue(false);

        http.get('url', {}, {})
            .catch((error: any) => {
                expect(error.title).toBe('LOYALTY_APP.ERROR.HTTP.REQUEST');
                expect(error.error).toBe('cordova_not_available');
                done();
            });
    });

    it('makes POST request', (done: Function) => {
        spyOn(http, 'isOffline').and.returnValue(false);

        http.post('url', {}, {})
            .catch((error: any) => {
                expect(error.title).toBe('LOYALTY_APP.ERROR.HTTP.REQUEST');
                expect(error.error).toBe('cordova_not_available');
                done();
            });
    });

    it('handles offline state', (done: Function) => {
        spyOn(http, 'isOffline').and.returnValue(true);

        http.get('url', {}, {})
            .catch((error: any) => {
                expect(error.title).toBe('LOYALTY_APP.ERROR.HTTP.OFFLINE.TITLE');
                expect(error.error).toBe('LOYALTY_APP.ERROR.HTTP.OFFLINE.TEXT');
                done();
            });
    });

    it('gets basic auth header', () => {
        spyOn(http.http, 'getBasicAuthHeader').and.callThrough();

        let header: any = http.getBasicAuthHeader('foo', 'bar');

        expect(http.http.getBasicAuthHeader).toHaveBeenCalledWith('foo', 'bar');
        expect(header.error).toBe('cordova_not_available');
    });

    it('uses basic auth header', () => {
        spyOn(http.http, 'useBasicAuth');

        http.useBasicAuth('foo', 'bar');

        expect(http.http.useBasicAuth).toHaveBeenCalledWith('foo', 'bar');
    });
});
