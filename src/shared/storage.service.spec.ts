import { StorageService } from './storage.service';
import { NativeStorage } from '@ionic-native/native-storage';

describe('Service: Storage', () => {

    let storage: StorageService;
    let warn: any;

    beforeAll(() => {
        // Disable warning due to cordova missing
        warn = console.warn;
        console.warn = (): void => {};
    });

    afterAll(() => {
        console.warn = warn;
    });

    beforeEach(() => {
        storage = new StorageService(
            (<any> new NativeStorage())
        );
    });

    it('initialises', () => {
        expect(storage).not.toBeNull();
    });

    it('sets item', (done: Function) => {
        storage.set('foo', 'bar')
            .catch((error: any) => {
                expect(error.title).toBe('LOYALTY_APP.ERROR.STORAGE.SET');
                expect(error.error).toBe('cordova_not_available');
                done();
            });
    });

    it('gets item', (done: Function) => {
        storage.get('foo')
            .catch((error: any) => {
                expect(error.title).toBe('LOYALTY_APP.ERROR.STORAGE.GET');
                expect(error.error).toBe('cordova_not_available');
                done();
            });
    });

    it('removes item', (done: Function) => {
        storage.remove('foo')
            .catch((error: any) => {
                expect(error.title).toBe('LOYALTY_APP.ERROR.STORAGE.REMOVE');
                expect(error.error).toBe('cordova_not_available');
                done();
            });
    });
});
