import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

@Injectable()
export class AlertService {
    loader: any;

    constructor(
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        public translate: TranslateService
    ) {

    }

    alert(title: string, subtitle: string): void {
        this.translate.get([
            title,
            subtitle,
            'LOYALTY_APP.COMMON.OK'
        ]).subscribe((translations: any) => {
            this.alertCtrl.create({
                title: translations[title],
                subTitle: translations[subtitle],
                buttons: [translations['LOYALTY_APP.COMMON.OK']]
            }).present();
        });
    }

    showLoading(): void {
        this.translate.get('LOYALTY_APP.COMMON.WAIT').subscribe((content: string) => {
            this.loader = this.loadingCtrl.create({
                content: content
            });

            this.loader.present();
        });
    }

    hideLoading(): void {
        if (this.loader) {
            this.loader.dismiss();
            this.loader = null;
        }
    }
}
