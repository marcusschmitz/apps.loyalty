export class StorageServiceMock {
    storage: any = {};

    set(reference: string, value: any): Promise<{}> {
        this.storage[reference] = value;

        return Promise.resolve(true);
    }

    get(reference: string): Promise<{}> {
        return Promise.resolve(this.storage[reference]);
    }

    remove(reference: string): Promise<{}> {
        delete(this.storage[reference]);

        return Promise.resolve(true);
    }
}
