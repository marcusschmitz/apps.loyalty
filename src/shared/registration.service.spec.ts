import { HTTP } from '@ionic-native/http';
import { Network } from '@ionic-native/network';

import { RegistrationService } from './registration.service';
import { AlertServiceMock } from './alert.service.mock';
import { AuthService } from './auth.service';
import { HttpServiceMock } from './http.service.mock';
import { StorageServiceMock } from '../shared/storage.service.mock';
import { UtilsService } from '../shared/utils.service';
import { TranslateMock, DatePipeMock, BarcodeScannerMock } from '../mocks';

describe('Service: Registration', () => {

    let registration: RegistrationService;
    let warn: any;

    beforeAll(() => {
        // Disable warning due to cordova missing
        warn = console.warn;
        console.warn = (): void => {};
    });

    afterAll(() => {
        console.warn = warn;
    });

    beforeEach(() => {
        registration = new RegistrationService(
            (<any> new AlertServiceMock()),
            (<any> new AuthService(
                (<any> new HttpServiceMock(
                    (<any> new HTTP()),
                    (<any> new Network())
                )),
                (<any> new StorageServiceMock()),
                (<any> new UtilsService())
            )),
            (<any> new BarcodeScannerMock()),
            (<any> new HttpServiceMock(
                (<any> new HTTP()),
                (<any> new Network())
            )),
            (<any> new TranslateMock()),
            (<any> new DatePipeMock())
        );
    });

    it('initialises', () => {
        expect(registration).not.toBeNull();
    });

    it('validates serial', () => {
        expect(registration.isSerialValid('21161700100127830052029642N1')).toBeTruthy();
        expect(registration.isSerialValid('21161700100127830052029640N9')).toBeTruthy();
        expect(registration.isSerialValid('21163400100127830052051589N3')).toBeTruthy();
        expect(registration.isSerialValid('')).toBeFalsy();
        expect(registration.isSerialValid('21161700100127830052029642N1A')).toBeFalsy();
        expect(registration.isSerialValid('21161700100127830052029642N2')).toBeFalsy();
        expect(registration.isSerialValid('21161700100127830052029642P1')).toBeFalsy();
    });

    it('scans', (done: Function) => {
        registration.scan()
            .then((barcode: string) => {
                expect(barcode).toBe('BARCODE');

                done();
            });
    });

    it('registers serial', (done: Function) => {
        let values: any = {
            serial: 'serial',
            street: 'street',
            houseNo: 'houseNo',
            zip: 'zip',
            city: 'city',
            firstName: 'firstname',
            lastName: 'lastname',
            phone: 'phone',
            email: 'email'
        };

        registration.register(values)
            .then((message: any) => {
                expect(message.title).toBe('LOYALTY_APP.REGISTRATION.MESSAGE.VALID.TITLE');
                expect(message.message).toBe('LOYALTY_APP.REGISTRATION.MESSAGE.VALID.TEXT');

                registration.auth.http['userInfoCall'] = 0;

                registration.register(values)
                    .catch((error: any) => {
                        expect(error.title).toBe('LOYALTY_APP.ERROR.HTTP.REQUEST');
                        expect(error.reason).toBe('Error reason');
                        expect(error.message).toBe('Error message');

                        done();
                    });
            });
    });

    it('gets registrations', (done: Function) => {
        registration.getRegistrations('foo', 'bar')
            .then((registrations: any) => {
                expect(registrations.length).toBe(3);
                expect(registrations[0].serialNumber).toBe('SERIAL1');
                expect(registrations[0].productName).toBe('PRODUCT1');
                expect(registrations[0].installationDate).toBe('DATE1');
                expect(registrations[1].serialNumber).toBe('SERIAL2');
                expect(registrations[1].productName).toBe('PRODUCT2');
                expect(registrations[1].installationDate).toBe('DATE2');
                expect(registrations[2].serialNumber).toBe('SERIAL3');
                expect(registrations[2].productName).toBe('PRODUCT3');
                expect(registrations[2].installationDate).toBe('DATE3');

                registration.auth.http['userInfoCall'] = 0;

                registration.getRegistrations('foo', 'bar')
                    .catch((error: any) => {
                        expect(error.title).toBe('LOYALTY_APP.ERROR.HTTP.REQUEST');
                        expect(error.reason).toBe('Error reason');
                        expect(error.message).toBe('Error message');

                        done();
                    });
            });
    });

    it('gets next registrations', () => {
        spyOn(registration, 'getRegistrations');

        registration.period = ['foo','bar'];

        registration.getNextRegistrations();

        expect(registration.getRegistrations).toHaveBeenCalledWith('bar', 'foo');
    });

    it('sets current date', () => {
        registration.setCurrentDate();

        expect(registration.date.toLocaleString()).toBe((new Date).toLocaleString());
    });

    it('sets period', () => {
        registration.date = new Date(2017, 3, 1, 12, 0, 0);
        registration.periodLength = 3;

        registration.setPeriod();

        expect(registration.period.length).toBe(3);
        expect(registration.period[0]).toBe('2017-04-01');
        expect(registration.period[1]).toBe('2017-03-01');
        expect(registration.period[2]).toBe('2017-02-01');

        registration.periodLength = 2;
        registration.setPeriod();

        expect(registration.period.length).toBe(2);
        expect(registration.period[0]).toBe('2017-02-01');
        expect(registration.period[1]).toBe('2017-01-01');
    });

    it('sets next period', () => {
        registration.date = new Date(2017, 3, 1, 12, 0, 0);
        registration.periodLength = 3;

        registration.setNextPeriod();

        expect(registration.period.length).toBe(3);
        expect(registration.period[0]).toBe('2017-03-01');
        expect(registration.period[1]).toBe('2017-02-01');
        expect(registration.period[2]).toBe('2017-01-01');
    });

    it('groups registrations', () => {
        registration.date = new Date(2017, 3, 1, 12, 0, 0);
        registration.periodLength = 3;

        registration.setPeriod();
        let groupedRegistrations: any = registration.groupRegistrations([
            {foo: 'bar1', installationDate: new Date(2017, 3, 1, 12, 0, 0)},
            {foo: 'bar2', installationDate: new Date(2017, 3, 1, 12, 0, 0)},
            {foo: 'bar3', installationDate: new Date(2017, 3, 1, 12, 0, 0)},
            {foo: 'bar4', installationDate: new Date(2017, 2, 1, 12, 0, 0)},
            {foo: 'bar5', installationDate: new Date(2017, 1, 1, 12, 0, 0)},
            {foo: 'bar6', installationDate: new Date(2017, 1, 1, 12, 0, 0)}
        ]);

        expect(groupedRegistrations.length).toBe(3);
        expect(groupedRegistrations[0].month).toBe('2017-04-01');
        expect(groupedRegistrations[0].items.length).toBe(3);
        expect(groupedRegistrations[0].items[0].foo).toBe('bar1');
        expect(groupedRegistrations[0].items[1].foo).toBe('bar2');
        expect(groupedRegistrations[0].items[2].foo).toBe('bar3');
        expect(groupedRegistrations[1].month).toBe('2017-03-01');
        expect(groupedRegistrations[1].items.length).toBe(1);
        expect(groupedRegistrations[1].items[0].foo).toBe('bar4');
        expect(groupedRegistrations[2].month).toBe('2017-02-01');
        expect(groupedRegistrations[2].items.length).toBe(2);
        expect(groupedRegistrations[2].items[0].foo).toBe('bar5');
        expect(groupedRegistrations[2].items[1].foo).toBe('bar6');
    });
});
