import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';

@Injectable()
export class StorageService {
    errors: any = {
        '1': 'NATIVE_WRITE_FAILED',
        '2': 'ITEM_NOT_FOUND',
        '3': 'NULL_REFERENCE',
        '4': 'UNDEFINED_TYPE',
        '5': 'JSON_ERROR',
        '6': 'WRONG_PARAMETER'
    }

    constructor(
        public nativeStorage: NativeStorage
    ) {

    }

    set(reference: string, value: any): Promise<{}> {
        return this.nativeStorage.setItem(reference, value)
            .catch((error: any) => {
                return Promise.reject({
                    title: 'LOYALTY_APP.ERROR.STORAGE.SET',
                    error: typeof error === 'string' ? error : this.errors[error.code]
                });
            });
    }

    get(reference: string): Promise<{}> {
        return this.nativeStorage.getItem(reference)
            .catch((error: any) => {
                return Promise.reject({
                    title: 'LOYALTY_APP.ERROR.STORAGE.GET',
                    error: typeof error === 'string' ? error : this.errors[error.code]
                });
            });
    }

    remove(reference: string): Promise<{}> {
        return this.nativeStorage.remove(reference)
            .catch((error: any) => {
                return Promise.reject({
                    title: 'LOYALTY_APP.ERROR.STORAGE.REMOVE',
                    error: typeof error === 'string' ? error : this.errors[error.code]
                });
            });
    }

}
