import { Injectable } from '@angular/core';

import { Config } from '../app/config';
import { HttpService } from './http.service';
import { StorageService } from './storage.service';
import { UtilsService } from './utils.service';

@Injectable()
export class AuthService {
    credentials: any = null;

    constructor(
        public http: HttpService,
        public storage: StorageService,
        public utils: UtilsService
    ) {

    }

    login(username: string, password: string): Promise<{}> {
        return this.getToken({
            grant_type: 'password',
            username: username.trim(),
            password: password.trim()
        }, 'basic');
    }

    logout(): Promise<void> {
        let promise: Promise<void> = this.storage.remove('credentials')
            .then(() => this.setCredentials(null));

        return promise;
    }

    isLoggedIn(): Promise<{}> {
        var self: AuthService = this;

        return new Promise((resolve: Function, reject: Function): void => {

            let dissolvePromise: Function = (): void => {
                self.areCredentialsValid()
                    .then(() => resolve())
                    .catch(() => reject());
            };

            if (self.credentials === null) {
                self.storage.get('credentials')
                    .then((credentials: any) => {
                        self.setCredentials(credentials);

                        dissolvePromise();
                    })
                    .catch(() => reject());
            } else {
                dissolvePromise();
            }
        });
    }

    getUserInfo(): Promise<{}> {
        return this.http.get(Config.url + '/uaa/oauth/userinfo', {}, {
            Authorization: 'Bearer ' + this.getAccessToken()
        });
    }

    getAccessToken(): string {
        return this.credentials ? this.credentials.access_token : '';
    }

    getRefreshToken(): string {
        return this.credentials ? this.credentials.refresh_token : '';
    }

    getHeader(auth: string, isJson: boolean): any {
        let header: any = {};

        if (auth === 'basic') {
            header = this.http.getBasicAuthHeader(Config.username, Config.password);
        } else if (auth === 'access') {
            header['Authorization'] = 'Bearer ' + this.getAccessToken();
        } else if (auth === 'refresh') {
            header['Authorization'] = 'Bearer ' + this.getRefreshToken();
        }

        Object.assign(header, {
            'X-Brand': Config.brand,
            'X-Country': Config.country,
            'X-Language': Config.lang,
            'X-Middleware-Token': Config.token
        });

        if (isJson) {
            header['Content-Type'] = 'application/json; charset=utf-8';
        }

        return header;
    }

    isLoyalty(userInfo: any): boolean {
        return (
            userInfo.user.account.loyalty !== undefined &&
            userInfo.user.account.loyalty !== null &&
            userInfo.user.account.loyalty.id !== undefined &&
            userInfo.user.account.loyalty.id !== null
        );
    }

    getToken(params: any, auth: string): Promise<{}> {
        var self: AuthService = this;

        return new Promise((resolve: Function, reject: Function): void => {

            let rejectIt: Function = (error: any): void => {
                self.setCredentials(null);
                reject(error);
            };

            self.http.post(Config.url + '/uaa/oauth/token', params, self.getHeader(auth, false))
                .then((credentials: any) => {
                    credentials.expires = self.utils.now() + credentials.expires_in;

                    self.setCredentials(credentials);

                    self.getUserInfo()
                        .then((userInfo: any) => {
                            if (self.isLoyalty(userInfo)) {
                                self.storage.set('credentials', credentials)
                                    .then(() => resolve())
                                    .catch((error: any) => rejectIt(error));
                            } else {
                                rejectIt({
                                    status: 0,
                                    title: 'LOYALTY_APP.LOGIN.MESSAGE.NOLOYALTY.TITLE',
                                    error: 'LOYALTY_APP.LOGIN.MESSAGE.NOLOYALTY.TEXT'
                                });
                            }
                        })
                        .catch((error: any) => rejectIt(error));
                })
                .catch((error: any) => {
                    if (error.status === 401) {
                        error.title = 'LOYALTY_APP.LOGIN.MESSAGE.INVALID.TITLE';
                        error.error = 'LOYALTY_APP.LOGIN.MESSAGE.INVALID.TEXT';
                    }

                    rejectIt(error);
                });
        });
    }

    setCredentials(credentials: any): void {
        this.credentials = credentials;
    }

    areCredentialsValid(): Promise<{}> {
        var self: AuthService = this;

        return new Promise((resolve: Function, reject: Function): void => {
            if (self.credentials !== null) {
                // Make delay of 30 seconds till token is expired
                if (self.credentials.expires >= self.utils.now() + 30) {
                    resolve();
                } else {
                    self.getToken({
                        grant_type: 'refresh_token'
                    }, 'refresh')
                        .then(() => resolve())
                        .catch(() => {
                            self.setCredentials(null);
                            reject();
                        });
                }
            } else {
                reject();
            }
        });
    }
}
