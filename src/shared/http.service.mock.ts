import { Config } from '../app/config';
import { HttpService } from './http.service';

export class HttpServiceMock extends HttpService {
    userInfoCall: number = 0;
    registrationCall: number = 0;
    getRegistrationsCall: number = 0;

    getBasicAuthHeader(username: string, password: string): any {
        return {
            Authorization: username + '//' + password
        };
    }

    useBasicAuth(username: string, password: string): void {

    }

    isOffline(): boolean {
        return false
    }

    request(type: string, url: string, params: any, headers: any): Promise<{}> {
        if (url === Config.url + '/uaa/oauth/token') {

            if (params.grant_type === 'refresh_token') {
                params.username = 'valid';
            }

            let validResponses: any = {
                valid: {
                    status: 200,
                    data: JSON.stringify({
                        'access_token': 'access_token',
                        'refresh_token': 'refresh_token',
                        'expires_in': 1000,
                        'token_type': 'token_type'
                    })
                }
            };

            let errorResponses: any = {
                invalid: {
                    status: 401
                },
                offline: {
                    status: 0,
                    title: 'LOYALTY_APP.ERROR.HTTP.OFFLINE.TITLE',
                    error: 'LOYALTY_APP.ERROR.HTTP.OFFLINE.TEXT'
                },
                error: {
                    status: 0,
                    title: 'LOYALTY_APP.ERROR.HTTP.REQUEST',
                    error: 'timeout'
                }
            };

            return new Promise((resolve: Function, reject: Function): void => {

                if (validResponses[params.username]) {
                    resolve(validResponses[params.username]);
                }

                if (errorResponses[params.username]) {
                    reject(errorResponses[params.username]);
                }
            });
        }

        if (url === Config.url + '/uaa/oauth/userinfo') {
            this.userInfoCall++;

            if (this.userInfoCall === 1) {
                return Promise.resolve({
                    data: JSON.stringify({
                        'firstname': 'John',
                        'lastname': 'Doe',
                        'user': {
                            'account': {
                                'id': 1,
                                'loyalty': {
                                    'id': 1
                                }
                            }
                        }
                    })
                });
            }

            if (this.userInfoCall === 2) {
                return Promise.resolve({
                    data: '{invalid JSON}'
                });
            }

            if (this.userInfoCall === 3) {
                return Promise.reject({
                    status: 0,
                    title: 'LOYALTY_APP.ERROR.HTTP.OFFLINE.TITLE',
                    error: 'LOYALTY_APP.ERROR.HTTP.OFFLINE.TEXT'
                });
            }
        }

        if (type === 'post' && url === Config.url + '/products-loyalty-registrations') {
            this.registrationCall++;

            if (this.registrationCall === 1) {
                return Promise.resolve({
                    data: ''
                });
            }

            if (this.registrationCall === 2) {
                return Promise.reject({
                    status: 500,
                    title: 'LOYALTY_APP.ERROR.HTTP.REQUEST',
                    error: JSON.stringify({
                        'reason': 'Error reason',
                        'message': 'Error message'
                    })
                });
            }
        }

        if (type === 'get' && url === Config.url + '/products-loyalty-registrations') {
            this.getRegistrationsCall++;

            if (this.getRegistrationsCall === 1) {
                return Promise.resolve({
                    data: JSON.stringify([
                        {
                            'serialNumber': 'SERIAL1',
                            'productName': 'PRODUCT1',
                            'installationDate': 'DATE1'
                        },
                        {
                            'serialNumber': 'SERIAL2',
                            'productName': 'PRODUCT2',
                            'installationDate': 'DATE2'
                        },
                        {
                            'serialNumber': 'SERIAL3',
                            'productName': 'PRODUCT3',
                            'installationDate': 'DATE3'
                        }
                    ])
                });
            }

            if (this.getRegistrationsCall === 2) {
                return Promise.reject({
                    status: 500,
                    title: 'LOYALTY_APP.ERROR.HTTP.REQUEST',
                    error: JSON.stringify({
                        'reason': 'Error reason',
                        'message': 'Error message'
                    })
                });
            }
        }
    }
}
