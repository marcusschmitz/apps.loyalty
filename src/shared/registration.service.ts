import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { TranslateService } from 'ng2-translate';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { Config } from '../app/config';
import { AlertService } from './alert.service';
import { AuthService } from './auth.service';
import { HttpService } from './http.service';

@Injectable()
export class RegistrationService {

    date: Date = new Date();
    periodLength: number = 3;
    period: Array<string> = [];

    constructor(
        public alert: AlertService,
        public auth: AuthService,
        public barcodeScanner: BarcodeScanner,
        public http: HttpService,
        public translate: TranslateService,
        public datepipe: DatePipe
    ) {

    }

    isSerialValid(serial: string): boolean {
        let valid: boolean = false;

        if (serial.length === 28) {
            let calculationStart: number = 1;
            let initialProveDigit: number = 0;

            for (let i: number = 0; i <= 26; i++) {
                let serialCharAscii: number  = serial.charCodeAt(i);
                let asciiFactor: number      = serialCharAscii - 32;
                let multipleNrFactor: number = calculationStart++ * asciiFactor;

                initialProveDigit += multipleNrFactor;
            }

            let calculatedProveDigit: string = (initialProveDigit % 10).toString();
            let realProveDigit: string       = serial.charAt(27);

            if (calculatedProveDigit === realProveDigit) {
                valid = true;
            }
        }

        return valid;
    }

    scan(): Promise<{}> {
        return new Promise((resolve: Function): void => {
            this.translate.get('LOYALTY_APP.REGISTRATION.BARCODE').subscribe((prompt: string) => {
                this.barcodeScanner.scan({ prompt: prompt })
                    .then((barcodeData: any) => resolve(barcodeData.text))
                    .catch((error: string) => {
                        this.alert.alert('LOYALTY_APP.ERROR.BARCODE.SCAN', error);
                        resolve('');
                    });
            });
        });
    }

    register(values: any): Promise<{}> {
        var self: RegistrationService = this;

        return new Promise((resolve: Function, reject: Function): void => {
            self.auth.getUserInfo()
                .then((userInfo: any) => {
                    self.http.post(Config.url + '/products-loyalty-registrations', {
                        installer: {
                            id: userInfo.user.account.id
                        },
                        device: {
                            serialNumber: values.serial.trim(),
                            installationDate: values.date
                        },
                        location: {
                            street: values.street.trim(),
                            houseNumber: values.houseNo.trim(),
                            postalCode: values.zip.trim(),
                            city: values.city.trim()
                        },
                        customer: {
                            firstName: values.firstName.trim(),
                            lastName: values.lastName.trim(),
                            phone: values.phone.trim(),
                            email: values.email.trim()
                        }
                    }, self.auth.getHeader('access', true))
                        .then(() => resolve({
                            title: 'LOYALTY_APP.REGISTRATION.MESSAGE.VALID.TITLE',
                            message: 'LOYALTY_APP.REGISTRATION.MESSAGE.VALID.TEXT'
                        }))
                        .catch((error: any) => reject(error));
                })
                .catch((error: any) => {
                    self.alert.alert(error.title, error.error);
                    reject(error);
                });
        });
    }

    getNextRegistrations(): Promise<[{}]> {
        return this.getRegistrations(this.period[this.period.length - 1], this.period[0]);
    }

    getRegistrations(from: string, to: string): Promise<[{}]> {
        return this.auth.getUserInfo()
            .then((userInfo: any) => {
                if (!this.auth.isLoyalty(userInfo)) {
                    return Promise.resolve([]);
                }

                return this.http.get(Config.url + '/products-loyalty-registrations', {
                    'loyaltyId' : userInfo.user.account.loyalty.id,
                    'startDate' : from,
                    'endDate' : to
                }, this.auth.getHeader('access', false));
            })
            .catch((error: any) => {
                this.alert.alert(error.title, error.error);
                return Promise.reject(error);
            });
    }

    setCurrentDate(): void {
        this.date = new Date();
    }

    setPeriod(): void {
        this.period = [];
        this.period.push(this.datepipe.transform(this.date, 'yyyy-MM'));

        for (let i: number = 1; i < this.periodLength; i++) {
            this.date.setMonth(this.date.getMonth() - 1);
            this.period.push(this.datepipe.transform(this.date, 'yyyy-MM'));
        }
    }

    setNextPeriod(): void {
        this.date.setMonth(this.date.getMonth() - 1);
        this.setPeriod();
    }

    groupRegistrations(registrations: any): any {
        let groupedRegistrations: Array<{}> = [];

        for (let i: number = 0; i < this.period.length; i++) {
            let month: string = this.period[i];
            let items: Array<{}> = [];

            for (let r: number = 0; r < registrations.length; r++) {
                let registration: any = registrations[r];
                let installationDate: string = this.datepipe.transform(registration.installationDate, 'yyyy-MM');

                if (installationDate === month) {
                    items.push(registration);
                }
            }

            groupedRegistrations.push({
                month: month,
                items: items
            });
        }

        return groupedRegistrations;
    }
}
