import { Injectable } from '@angular/core';

@Injectable()
export class UtilsService {

    now(): number {
        return Math.floor(Date.now() / 1000);
    }

    getLinkParts(str: string): Array<string> {
        let firstIndex: number = str.indexOf('[');

        if (firstIndex === -1 || firstIndex === str.length - 1) {
            return [str, '', ''];
        }

        let firstPart: string   = str.substr(0, firstIndex);
        let secondPart: string  = str.substr(firstIndex + 1);
        let secondIndex: number = secondPart.indexOf(']');

        if (secondIndex === -1) {
            return [str, '', ''];
        }

        return [
            firstPart,
            secondPart.substr(0, secondIndex),
            secondIndex === secondPart.length - 1 ? '' : secondPart.substr(secondIndex + 1)
        ];
    }

    isEmailValid(email: string): boolean {
        let regExp: any = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regExp.test(email);
    }
}
