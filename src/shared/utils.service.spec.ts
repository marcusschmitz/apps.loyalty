import { UtilsService } from './utils.service';

describe('Service: Utils', () => {

    let utils: UtilsService;

    beforeEach(() => {
        utils = new UtilsService();
    });

    it('initialises', () => {
        expect(utils).not.toBeNull();
    });

    it('get link parts', () => {
        let parts: Array<string>;

        parts = utils.getLinkParts('foo [bar] foobar');

        expect(parts.length).toBe(3);
        expect(parts[0]).toBe('foo ');
        expect(parts[1]).toBe('bar');
        expect(parts[2]).toBe(' foobar');

        parts = utils.getLinkParts('[bar] foobar');

        expect(parts.length).toBe(3);
        expect(parts[0]).toBe('');
        expect(parts[1]).toBe('bar');
        expect(parts[2]).toBe(' foobar');

        parts = utils.getLinkParts('foo [bar]');

        expect(parts.length).toBe(3);
        expect(parts[0]).toBe('foo ');
        expect(parts[1]).toBe('bar');
        expect(parts[2]).toBe('');

        parts = utils.getLinkParts('foo bar] foobar');

        expect(parts.length).toBe(3);
        expect(parts[0]).toBe('foo bar] foobar');
        expect(parts[1]).toBe('');
        expect(parts[2]).toBe('');

        parts = utils.getLinkParts('foo [bar foobar');

        expect(parts.length).toBe(3);
        expect(parts[0]).toBe('foo [bar foobar');
        expect(parts[1]).toBe('');
        expect(parts[2]).toBe('');

        parts = utils.getLinkParts('foo [ba[r] foobar');

        expect(parts.length).toBe(3);
        expect(parts[0]).toBe('foo ');
        expect(parts[1]).toBe('ba[r');
        expect(parts[2]).toBe(' foobar');

        parts = utils.getLinkParts('foo [ba]r] foobar');

        expect(parts.length).toBe(3);
        expect(parts[0]).toBe('foo ');
        expect(parts[1]).toBe('ba');
        expect(parts[2]).toBe('r] foobar');

        parts = utils.getLinkParts('[foo] [bar] [foobar]');

        expect(parts.length).toBe(3);
        expect(parts[0]).toBe('');
        expect(parts[1]).toBe('foo');
        expect(parts[2]).toBe(' [bar] [foobar]');
    });

    it('validates email', () => {
        expect(utils.isEmailValid('')).toBeFalsy();
        expect(utils.isEmailValid('foo')).toBeFalsy();
        expect(utils.isEmailValid('foo@bar')).toBeFalsy();
        expect(utils.isEmailValid('foo@bar@foo.bar')).toBeFalsy();
        expect(utils.isEmailValid('foo.bar')).toBeFalsy();
        expect(utils.isEmailValid('foo@bar.foo')).toBeTruthy();
        expect(utils.isEmailValid('foo.bar@foo.bar')).toBeTruthy();
        expect(utils.isEmailValid('foo+bar@foo.bar')).toBeTruthy();
        expect(utils.isEmailValid('foo@bar.foo.bar')).toBeTruthy();
    });
});
