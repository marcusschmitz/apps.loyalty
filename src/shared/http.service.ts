import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http';
import { Network } from '@ionic-native/network';

@Injectable()
export class HttpService {

    constructor(
        public http: HTTP,
        public network: Network
    ) {

    }

    get(url: string, params: any, headers: any): Promise<{}> {
        return this.request('get', url, params, headers)
            .then((data: any) => this.resolve(data))
            .catch((error: any) => this.reject(error));
    }

    post(url: string, params: any, headers: any): Promise<{}> {
        return this.request('post', url, params, headers)
            .then((data: any) => this.resolve(data))
            .catch((error: any) => this.reject(error));
    }

    getBasicAuthHeader(username: string, password: string): any {
        return this.http.getBasicAuthHeader(username, password)
    }

    useBasicAuth(username: string, password: string): void {
        this.http.useBasicAuth(username, password);
    }

    isOffline(): boolean {
        return this.network.type === 'none';
    }

    request(type: string, url: string, params: any, headers: any): Promise<{}> {
        if (this.isOffline()) {
            return Promise.reject({
                status: 0,
                title: 'LOYALTY_APP.ERROR.HTTP.OFFLINE.TITLE',
                error: 'LOYALTY_APP.ERROR.HTTP.OFFLINE.TEXT'
            });
        }

        return this.http[type](url, params, headers);
    }

    private resolve(data: any): Promise<{}> {

        if (data.data === '') {
            return Promise.resolve(null);
        }

        try {
            data = JSON.parse(data.data);
        } catch(e) {
            return Promise.reject({
                status: 0,
                title: 'LOYALTY_APP.ERROR.HTTP.REQUEST',
                error: 'LOYALTY_APP.ERROR.JSON.PARSE'
            });
        }

        return Promise.resolve(data);
    }

    private reject(error: any): Promise<{}> {

        if (typeof error === 'string') {
            error = {
                status: 0,
                error: error
            };
        } else {
            try {
                Object.assign(error, JSON.parse(error.error));
            } catch(e) {}
        }

        if (!error.hasOwnProperty('title')) {
            error.title = 'LOYALTY_APP.ERROR.HTTP.REQUEST';
        }

        return Promise.reject(error);
    }
}
