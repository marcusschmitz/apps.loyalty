import { AlertControllerMock, LoadingControllerMock, TranslateMock } from '../mocks';

import { AlertService } from './alert.service';

describe('Service: Alert', () => {

    let alert: AlertService;
    let warn: any;

    beforeAll(() => {
        // Disable warning due to cordova missing
        warn = console.warn;
        console.warn = (): void => {};
    });

    afterAll(() => {
        console.warn = warn;
    });

    beforeEach(() => {
        alert = new AlertService(
            (<any> new AlertControllerMock()),
            (<any> new LoadingControllerMock()),
            (<any> new TranslateMock())
        );
    });

    it('initialises', () => {
        expect(alert).not.toBeNull();
    });

    it('alerts', () => {
        spyOn(alert.alertCtrl, 'create').and.returnValue({
            present: (): void => {}
        });

        alert.alert('title', 'subtitle');

        expect(alert.alertCtrl.create).toHaveBeenCalledWith({
            title: 'title',
            subTitle: 'subtitle',
            buttons: ['LOYALTY_APP.COMMON.OK']
        });
    });

    it('shows loading', () => {
        spyOn(alert.loadingCtrl, 'create').and.returnValue({
            present: (): void => {}
        });

        alert.showLoading();

        expect(alert.loadingCtrl.create).toHaveBeenCalled();
    });

    it('hides loading', () => {
        spyOn(alert.loadingCtrl, 'create').and.returnValue({
            present: (): void => {},
            dismiss: (): void => {}
        });

        alert.showLoading();

        expect(alert.loader).not.toBeNull();

        alert.hideLoading();

        expect(alert.loader).toBeNull();
    });
});
