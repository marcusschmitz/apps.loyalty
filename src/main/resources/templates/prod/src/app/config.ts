export const Config: any = {
    version: '${app.version}',
    country: '${app.country}',
    lang: '${app.language}',
    locale: '${app.target.locale}',
    brand: '${app.brand.prod}',
    username: '${app.username}',
    password: '${app.password}',
    token: '${app.token.prod}',
    url: '${app.url.prod}'
}
