export class ConfigMock {

    public get(): any {
        return '';
    }

    public getBoolean(): boolean {
        return true;
    }

    public getNumber(): number {
        return 1;
    }

    public set(): any {
        return '';
    }
}

export class FormMock {
    public register(): any {
        return true;
    }
}

export class NavMock {

    public pop(): any {
        return new Promise((resolve: Function): void => {
            resolve();
        });
    }

    public push(): any {
        return new Promise((resolve: Function): void => {
            resolve();
        });
    }

    public getActive(): any {
        return {
            'instance': {
                'model': 'something',
            },
        };
    }

    public setRoot(): any {
        return true;
    }
}

export class PlatformMock {
    public ready(): Promise<{}> {
        return new Promise((resolve: Function): void => {
            resolve('READY');
        });
    }

    public registerBackButtonAction(fn: Function, priority?: number): Function {
        return ((): boolean => true);
    }

    public hasFocus(ele: HTMLElement): boolean {
        return true;
    }

    public doc(): HTMLDocument {
        return document;
    }

    public registerListener(ele: any, eventName: string, callback: any): Function {
        return ((): boolean => true);
    }

    public win(): Window {
        return window;
    }

    public raf(callback: any): number {
        return 1;
    }
}

export class MenuMock {
    public close(): any {
        return new Promise((resolve: Function): void => {
            resolve();
        });
    }
}

export class TranslateMock {
    public get(params: any): any {
        let translations: any;

        if (typeof params === 'string') {
            translations = params;
        } else {
            translations = {};

            params.forEach((item: string) => {
                translations[item] = item;
            });
        }

        return {
            subscribe: (func: Function): void => {
                func(translations);
            }
        }
    }

    public setDefaultLang(): void {

    }

    public use(): void {

    }
}

export class DatePipeMock {
    public transform(date: Date, format: string): string {
        return date.toISOString().split('T')[0];
    }
}

export class BarcodeScannerMock {
    public scan(prompt: string): Promise<{}> {
        return new Promise((resolve: Function): void => {
            resolve({
                text: 'BARCODE'
            });
        });
    }
}

export class StatusBarMock {
    public styleDefault(): void {

    }
}

export class SplashScreenMock {
    public hide(): void {

    }
}

export class AlertControllerMock {
    public create(): any {
        return this;
    }

    public present(): void {

    }
}

export class LoadingControllerMock {
    public create(): any {
        return this;
    }

    public present(): void {

    }
}
