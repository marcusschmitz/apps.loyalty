import { Component } from '@angular/core';
import { TranslateService } from 'ng2-translate';

@Component({
    selector: 'page-contact',
    templateUrl: 'contact.html'
})
export class ContactPage {

    email: string = '';
    phone: string = '';

    constructor(
        private translate: TranslateService
    ) {
        this.translate.get([
            'LOYALTY_APP.CONTACT.MAIL',
            'LOYALTY_APP.CONTACT.PHONE'
        ]).subscribe((translations: any) => {
            this.email = translations['LOYALTY_APP.CONTACT.MAIL'],
            this.phone = translations['LOYALTY_APP.CONTACT.PHONE']
        });
    }
}
