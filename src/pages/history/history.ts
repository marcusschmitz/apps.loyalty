import { Component, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { Content, NavController } from 'ionic-angular';

import { AlertService } from '../../shared/alert.service';
import { AuthService } from '../../shared/auth.service';
import { RegistrationService } from '../../shared/registration.service';

import { LoginPage } from '../../pages/login/login';
import { HistoryDetailPage } from '../../pages/history/detail/detail';

@Component({
    selector: 'page-history',
    templateUrl: 'history.html'
})
export class HistoryPage implements AfterViewChecked {
    @ViewChild(Content) content: Content;
    @ViewChild('itemGroupWrapper') itemGroupWrapper: ElementRef;

    registrations: Array<{}> = [];
    showMore: boolean = false;

    constructor(
        private alert: AlertService,
        private auth: AuthService,
        private registration: RegistrationService,
        public navCtrl: NavController
    ) {
        this.registration.setCurrentDate();
        this.registration.setPeriod();
        this.load();
    }

    getNextRegistrations(finish: Function): void {
        this.auth.isLoggedIn()
            .then(() => {
                this.registration.getNextRegistrations()
                    .then((registrations: Array<{}>) => {
                        finish();

                        this.registrations = this.registrations.concat(
                            this.registration.groupRegistrations(registrations)
                        );

                        this.registration.setNextPeriod();
                    })
                    .catch((error: any) => {
                        finish();
                        this.alert.alert(error.title, error.error);
                    });
            })
            .catch(() => {
                finish();
                this.navCtrl.setRoot(LoginPage);
            });
    }

    loadInfinite(infiniteScroll: any): void {
        this.getNextRegistrations(() => {
            infiniteScroll.complete();
        });
    }

    load(): void {
        this.alert.showLoading();
        this.getNextRegistrations(() => {
            this.alert.hideLoading();
        });
    }

    showDetails(registration: any): void {
        this.navCtrl.push(HistoryDetailPage, registration);
    }

    ngAfterViewChecked(): void {
        let dimensions: any = this.content.getContentDimensions();
        let groupWrapperHeight: number = this.itemGroupWrapper.nativeElement.offsetHeight;

        this.showMore = this.registrations.length && groupWrapperHeight <= dimensions.contentHeight;
    }
}
