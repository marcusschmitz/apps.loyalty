import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

@Component({
    selector: 'page-history-detail',
    templateUrl: 'detail.html'
})
export class HistoryDetailPage {
    registration: any;
    format: string;

    constructor(
        private navParams: NavParams,
        private translate: TranslateService
    ) {
        this.translate.get('LOYALTY_APP.COMMON.DATEFORMAT').subscribe((format: string) => {
            this.format = format;
            this.registration = navParams.data;
        });
    }
}
