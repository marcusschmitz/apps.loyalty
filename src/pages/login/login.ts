import { Component } from '@angular/core';
import { ModalController, Nav } from 'ionic-angular';
import { Validators, FormBuilder } from '@angular/forms';
import { TranslateService } from 'ng2-translate';

import { AlertService } from '../../shared/alert.service';
import { AuthService } from '../../shared/auth.service';
import { UtilsService } from '../../shared/utils.service';
import { LoginTermsPage } from './terms/terms';
import { RegistrationPage } from '../registration/registration';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage {
    credentials: any;

    termsParts: any = ['', '', ''];

    constructor(
        private formBuilder: FormBuilder,
        private modalCtrl: ModalController,
        private nav: Nav,
        private translate: TranslateService,
        private alert: AlertService,
        private auth: AuthService,
        private utils: UtilsService
    ) {
        this.credentials = this.formBuilder.group({
            userName: ['', Validators.required],
            password: ['', Validators.required],
            termsAccepted: [false, Validators.pattern('true')]
        });

        this.setTermParts();
        this.translate.onLangChange.subscribe(() => this.setTermParts());
    }

    setTermParts(): void {
        this.translate.get('LOYALTY_APP.LOGIN.FORM.TERMS').subscribe((terms: string) => {
            this.termsParts = this.utils.getLinkParts(terms);
        });
    }

    openTerms(): void {
        let modal: any = this.modalCtrl.create(LoginTermsPage);
        modal.present();
    }

    onSubmit(): void {
        this.alert.showLoading();

        this.auth.login(
            this.credentials.value.userName,
            this.credentials.value.password
        )
            .then(() => {
                this.alert.hideLoading();
                this.nav.setRoot(RegistrationPage)
            })
            .catch((error: any) => {
                this.alert.hideLoading();
                this.alert.alert(error.title, error.error)
            });
    }
}
