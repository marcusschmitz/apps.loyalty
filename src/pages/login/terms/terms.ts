import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

@Component({
    selector: 'page-login-terms',
    templateUrl: 'terms.html'
})
export class LoginTermsPage {

     constructor(public viewCtrl: ViewController) {

     }

    dismiss(): void {
         this.viewCtrl.dismiss();
    }
}
