import { Component } from '@angular/core';
import { Validators, FormBuilder, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { NavController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

import { AlertService } from '../../shared/alert.service';
import { AuthService } from '../../shared/auth.service';
import { RegistrationService } from '../../shared/registration.service';
import { UtilsService } from '../../shared/utils.service';

import { LoginPage } from '../../pages/login/login';

@Component({
    selector: 'page-registration',
    templateUrl: 'registration.html'
})
export class RegistrationPage {
    registrationForm: any;

    headlineRegistration: string;
    headlineCustomer: string;

    serial: string;
    serialLength: number;

    currentDate: string;

    constructor(
        private alert: AlertService,
        private auth: AuthService,
        private utils: UtilsService,
        private registration: RegistrationService,
        private formBuilder: FormBuilder,
        private datePipe: DatePipe,
        public navCtrl: NavController,
        public translate: TranslateService
    ) {
        this.serial = '';
        this.serialLength = 0;

        this.currentDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');

        this.translate.get([
            'LOYALTY_APP.REGISTRATION.FORM.HEADLINE.REGISTRATION',
            'LOYALTY_APP.REGISTRATION.FORM.HEADLINE.CUSTOMER',
            'LOYALTY_APP.REGISTRATION.FORM.VALIDATION.ZIP',
            'LOYALTY_APP.REGISTRATION.FORM.VALIDATION.PHONE'
        ]).subscribe((translations: any) => {
            this.headlineRegistration = translations['LOYALTY_APP.REGISTRATION.FORM.HEADLINE.REGISTRATION'];
            this.headlineCustomer     = translations['LOYALTY_APP.REGISTRATION.FORM.HEADLINE.CUSTOMER'];

            let regExpZip: string   = translations['LOYALTY_APP.REGISTRATION.FORM.VALIDATION.ZIP'];
            let regExpPhone: string = translations['LOYALTY_APP.REGISTRATION.FORM.VALIDATION.PHONE'];

            let validatorZip: any;
            let validatorPhone: any;

            if (regExpZip !== '') {
                validatorZip = Validators.pattern(regExpZip);
            }

            if (regExpZip !== '') {
                validatorPhone = Validators.pattern(regExpPhone);
            }

            this.registrationForm = this.formBuilder.group({
                serial: ['', (control: FormControl): any => { return this.validateSerial(control); }],
                date: [this.currentDate, (control: FormControl): any => { return this.validateDate(control); }],
                firstName: '',
                lastName: '',
                street: '',
                houseNo: '',
                zip: ['', validatorZip],
                city: '',
                phone: ['', validatorPhone],
                email: ['', (control: FormControl): any => { return this.validateEmail(control); }],
            });
        });
    }

    validateSerial(control: FormControl): any {
        return this.registration.isSerialValid(control.value) ? null : {
            validateSerial: {
                valid: false
            }
        };
    }

    validateDate(control: FormControl): any {
        return (new Date(control.value) <= new Date()) ? null : {
            validateDate: {
                valid: false
            }
        };
    }

    validateEmail(control: FormControl): any {
        if (control.value === '') {
            return null;
        }

        return this.utils.isEmailValid(control.value) ? null : {
            validateEmail: {
                valid: false
            }
        };
    }

    checkSerial(): void {
        this.serialLength = this.serial.length;

        if (this.serialLength > 28) {
            this.serial = this.serial.substr(0, 28);
            this.serialLength = 28;
        }
    }

    scan(): void {
        this.registration.scan().then((serial: string) => {
            if (serial === '') {
                return;
            }

            this.serial = serial;
            this.checkSerial();
        });
    }

    onSubmit(): void {
        this.alert.showLoading();

        this.auth.isLoggedIn()
            .then(() => {
                this.registration.register(this.registrationForm.value)
                        .then((data: any) => {
                            this.alert.hideLoading();
                            this.alert.alert(data.title, data.message)
                        })
                        .catch((error: any) => {
                            this.alert.hideLoading();
                            this.alert.alert(error.title, error.message ? error.message : error.error)
                        });
            })
            .catch(() => {
                this.alert.hideLoading();
                this.navCtrl.setRoot(LoginPage)
            });
    }
}
