import { NgModule, ErrorHandler } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Http, HttpModule } from "@angular/http";
import { BrowserModule } from '@angular/platform-browser';
import { TranslateModule } from 'ng2-translate';
import { TranslateLoader, TranslateStaticLoader } from 'ng2-translate/src/translate.service';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NativeStorage } from '@ionic-native/native-storage';
import { Network } from '@ionic-native/network';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { VEPScan } from './app.component';
import { AlertService } from '../shared/alert.service';
import { AuthService } from '../shared/auth.service';
import { HttpService } from '../shared/http.service';
import { RegistrationService } from '../shared/registration.service';
import { StorageService } from '../shared/storage.service';
import { UtilsService } from '../shared/utils.service';

import { ContactPage } from '../pages/contact/contact';
import { HistoryPage } from '../pages/history/history';
import { HistoryDetailPage } from '../pages/history/detail/detail';
import { LoginPage } from '../pages/login/login';
import { LoginTermsPage } from '../pages/login/terms/terms';
import { RegistrationPage } from '../pages/registration/registration';

export function createTranslateLoader(http: Http): TranslateStaticLoader {
    return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

@NgModule({
    declarations: [
        VEPScan,
        ContactPage,
        HistoryPage,
        HistoryDetailPage,
        LoginPage,
        LoginTermsPage,
        RegistrationPage
    ],
    imports: [
        BrowserModule,
        HttpModule,
        TranslateModule.forRoot({
            provide: TranslateLoader,
            useFactory: createTranslateLoader,
            deps: [Http]
        }),
        IonicModule.forRoot(VEPScan)
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        VEPScan,
        ContactPage,
        HistoryPage,
        HistoryDetailPage,
        LoginPage,
        LoginTermsPage,
        RegistrationPage
    ],
    providers: [
        AlertService,
        AuthService,
        BarcodeScanner,
        DatePipe,
        HTTP,
        HttpService,
        NativeStorage,
        Network,
        RegistrationService,
        SplashScreen,
        StatusBar,
        StorageService,
        UtilsService,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {}
