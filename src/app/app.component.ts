import { Component, ViewChild } from '@angular/core';
import { TranslateService } from 'ng2-translate';
import { Platform, MenuController, Nav, Config as AngularConfig } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AlertService } from '../shared/alert.service';
import { AuthService } from '../shared/auth.service';

import { ContactPage } from '../pages/contact/contact';
import { HistoryPage } from '../pages/history/history';
import { LoginPage } from '../pages/login/login';
import { RegistrationPage } from '../pages/registration/registration';

import { Config } from './config';

@Component({
    templateUrl: 'app.html'
})
export class VEPScan {
    @ViewChild(Nav) nav: Nav;

    version: string;

    rootPage: any;
    pages: Array<{title: string, component: any}>;

    constructor(
        public platform: Platform,
        public statusBar: StatusBar,
        public splashScreen: SplashScreen,
        public menu: MenuController,
        public translate: TranslateService,
        public config: AngularConfig,
        public alert: AlertService,
        public auth: AuthService
    ) {
        let locale: string = "";

        this.version = Config.version;

        if (Config.locale) {
            locale = Config.locale;
        } else {
            locale = Config.lang + '_' + Config.country;
        }

        this.translate.setDefaultLang(Config.lang);
        this.translate.use('loyalty_' + Config.locale);

        this.initializeApp();
    }

    initializeApp(): void {
        this.platform.ready().then(() => {
            this.translate.get([
                'LOYALTY_APP.COMMON.BACK',
                'LOYALTY_APP.REGISTRATION.TITLE',
                'LOYALTY_APP.HISTORY.TITLE',
                'LOYALTY_APP.CONTACT.TITLE'
            ]).subscribe((translations: any) => {
                this.config.set('backButtonText', translations['LOYALTY_APP.COMMON.BACK']);

                this.pages = [
                    { title: translations['LOYALTY_APP.REGISTRATION.TITLE'], component: RegistrationPage },
                    { title: translations['LOYALTY_APP.HISTORY.TITLE'], component: HistoryPage },
                    { title: translations['LOYALTY_APP.CONTACT.TITLE'], component: ContactPage }
                ];

                this.auth.isLoggedIn()
                    .then(() => this.ready(RegistrationPage))
                    .catch(() => this.ready(LoginPage));
            });
        });
    }

    openPage(page: any): void {
        this.menu.close();
        this.nav.setRoot(page.component);
    }

    logout(): void {
        this.auth.logout()
            .then(() => {
                this.menu.close();
                this.nav.setRoot(LoginPage);
            })
            .catch((error: any) => this.alert.alert(error.title, error.error));
    }

    private ready(page: any): void {
        this.rootPage = page;

        this.statusBar.styleDefault();
        this.splashScreen.hide();
    }
}
