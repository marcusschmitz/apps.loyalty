import { VEPScan } from './app.component';
import { HTTP } from '@ionic-native/http';
import { MenuMock, NavMock, PlatformMock, TranslateMock, ConfigMock, StatusBarMock, SplashScreenMock } from '../mocks';
import { AlertServiceMock } from '../shared/alert.service.mock';
import { AuthService } from '../shared/auth.service';
import { HttpServiceMock } from '../shared/http.service.mock';
import { StorageServiceMock } from '../shared/storage.service.mock';
import { UtilsService } from '../shared/utils.service';
import { LoginPage } from '../pages/login/login';

describe('VEPScan', () => {

    let instance: VEPScan;
    let warn: any;

    beforeAll(() => {
        // Disable warning due to cordova missing
        warn = console.warn;
        console.warn = (): void => {};
    });

    afterAll(() => {
        console.warn = warn;
    });

    beforeEach(() => {
        instance = new VEPScan(
            (<any> new PlatformMock()),
            (<any> new StatusBarMock()),
            (<any> new SplashScreenMock()),
            (<any> new MenuMock()),
            (<any> new TranslateMock()),
            (<any> new ConfigMock()),
            (<any> new AlertServiceMock()),
            (<any> new AuthService(
                (<any> new HttpServiceMock(
                    (<any> new HTTP()),
                    (<any> new TranslateMock())
                )),
                (<any> new StorageServiceMock()),
                (<any> new UtilsService())
            ))
        );

        instance.nav = (<any>new NavMock());
    });

    it('initialises', () => {
        expect(instance).not.toBeNull();
    });

    it('initialises app', (done: Function) => {
        spyOn(instance.auth, 'isLoggedIn').and.callThrough();

        instance.initializeApp();

        setTimeout(() => {
            expect(instance.auth.isLoggedIn).toHaveBeenCalled();
            done();
        }, 1);
    });

    it('sets pages', (done: Function) => {
        setTimeout(() => {
            expect(instance.pages.length).toBeGreaterThan(0);
            done();
        }, 1);
    });

    it('opens a page', (done: Function) => {
        spyOn(instance.menu, 'close');
        spyOn(instance.nav, 'setRoot');

        setTimeout(() => {
            instance.openPage(instance.pages[0]);

            expect(instance.menu.close).toHaveBeenCalled();
            expect(instance.nav.setRoot).toHaveBeenCalled();
            done();
        }, 1);
    });

    it('logs out', (done: Function) => {
        spyOn(instance.menu, 'close');
        spyOn(instance.nav, 'setRoot');

        instance.logout();

        setTimeout(() => {
            expect(instance.menu.close).toHaveBeenCalled();
            expect(instance.nav.setRoot).toHaveBeenCalledWith(LoginPage);

            done();
        }, 1);
    });
});
